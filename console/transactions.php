<?php


require '../Conf/Connection.php';

session_start();

if(!isset($_SESSION['user'])) {
	header("Location: ../");
}

if($_GET['logout'] == 'true') {
	unset($_SESSION['user']);
	header("Location: ../");
}

$sid = $_SESSION['user'];
$query = "select * from users where SID='$sid'";
$results = $conn->query($query) or die("Invalid user");
$email = $results->fetch_assoc()['email'];
$query = "select * from transactions where email='$email'";
// $query = "select * from transactions"; //For testing purposes
$results = $conn->query($query) or die("Error executing commands");

function transform($input) {
	switch($input) {
		case "cashout":
		return "deposit";
		break;

		case "cashin":
		return "withdraw";
		break;

		default:
		return $input;
		break;
	}
}

 ?>
<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">

	    <!-- CSS -->
	    <!-- Add Material font (Roboto) and Material icon as needed -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	    <title>Transaction History | AurbanPay</title>

	    <!-- Add Material CSS, replace Bootstrap CSS -->
	    <link href="../css/main.css" rel="stylesheet">

	</head>

	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="../">AurbanPay</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link active" href=".">Dashboard <span class="sr-only">(current)</span></a>
		      </li>
		      <!-- <li class="nav-item disable">
		        <a class="nav-link" href="../Docs/">Documentation</a>
		      </li> -->
		      <!-- <li class="nav-item">
		        <a class="nav-link disabled" href="#">Services</a>
		      </li> -->
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Account
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="#">Settings</a>
		          <a class="dropdown-item" href="#">Help</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="./?logout=true">Logout</a>
		        </div>
		      </li>
		    </ul>
		  </div>

		</nav>

		<br>
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-12">
					<br>
					<div class="row">
						<div class="col-sm-2">
							<div class="row">
							  <div class="col-sm-12">
									<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
										<a class="nav-link" href="index.php" aria-selected="true">Dashboard</a>
										<a class="nav-link active" href="transactions.php" aria-selected="false">Transactions</a>
										<!--<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
										<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> -->
									</div>
									<hr>
									<?php
										if($results->num_rows > 0) {
									 ?>
							    <div class="list-group" id="list-tab" role="tablist">
							      	<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">All</a>
							      	<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Successful</a>
							      	<a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Failed</a>
							      	<a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Deposit</a>
									<a class="list-group-item list-group-item-action" id="list-cashin-list" data-toggle="list" href="#list-cashin" role="tab" aria-controls="cashin">Withdraw</a>
									<a class="list-group-item list-group-item-action" id="list-cashin-list" data-toggle="list" href="#list-transfer" role="tab" aria-controls="cashin">Transfer</a>
							    </div>
									<?php
										}
									?>
							  </div>
							</div>
						</div>
						<div class="col-sm-10">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
									<div class="card">
										<?php
										if($results->num_rows < 1) {
										?>
										<div class="card-header">
											<p  id="veryThin">No Transactions <small id="veryThinSmall">- Listing would be done once you do</small></p>
										</div>
										<?php } else {?>
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- All</small></p>
											<!-- <form>
											  <div class="row">
											    <div class="col">
											      <input type="text" class="form-control" placeholder="First name">
											    </div>
											    <div class="col">
											      <input type="text" class="form-control" placeholder="Last name">
											    </div>
													<div class="col">
														<button onclick="filter()" class="btn btn-afka">Filter</button>
													</div>
											  </div>
											</form> -->
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<div class="table-responsive">
														<table class="table">
													  <thead class="thead-dark">
													    <tr>
													      <th scope="col">#</th>
													      <th scope="col">Operation</th>
													      <th scope="col">Amount</th>
													      <th scope="col">Date</th>
													      <th scope="col">State</th>
																<th scope="col">Client</th>
																<th scope="col">Tracker ID</th>
													    </tr>
													  </thead>
													  <tbody>
															<?php
															$count = 1;
															while($row = $results->fetch_assoc()) {
															?>
													    <tr>
													      <th scope="row"><?php echo $count; ?></th>
													      <td><?php echo transform($row['type']); ?></td>
													      <td><?php echo $row['amount']; ?></td>
													      <td><?php echo $row['date']; ?></td>
													      <td><?php echo $row['state']; ?></td>
																<td><?php echo $row['email']; ?></td>
																<td><?php echo $row['trackerID']; ?></td>
													    </tr>
															<?php $count++; } ?>
													  </tbody>
													</table>
													</div>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
										<?php } ?>
									</div>
								</div>
								<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
									<div class="card">
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- Successful</small></p>
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<table class="table">
														<thead class="thead-dark">
															<tr>
																<th scope="col">#</th>
																<th scope="col">Operation</th>
																<th scope="col">Amount</th>
																<th scope="col">Date</th>
																<th scope="col">Client</th>
																<th scope="col">Tracker ID</th>
															</tr>
														</thead>
														<tbody>
															<?php
															$count = 1;
															$results->data_seek(0);
															while($row = $results->fetch_assoc()) {
																if($row['state'] == 'confirmed') {
															?>
															<tr>
																<th scope="row"><?php echo $count; ?></th>
																<td><?php echo transform($row['type']); ?></td>
																<td><?php echo $row['amount']; ?></td>
																<td><?php echo $row['date']; ?></td>
																<td><?php echo $row['email']; ?></td>
																<td><?php echo $row['trackerID']; ?></td>
															</tr>
															<?php $count++;  }
															} ?>
														</tbody>
													</table>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
									<div class="card">
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- Failed</small></p>
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<table class="table">
											  <thead class="thead-dark">
											    <tr>
											      <th scope="col">#</th>
											      <th scope="col">Operation</th>
											      <th scope="col">Amount</th>
											      <th scope="col">Date</th>
											      <th scope="col">Client</th>
														<th scope="col">Tracker ID</th>
											    </tr>
											  </thead>
											  <tbody>
													<?php
													$count = 1;
													$results->data_seek(0);
													while($row = $results->fetch_assoc()) {
														if($row['state'] == 'failed') {
													?>
													<tr>
														<th scope="row"><?php echo $count; ?></th>
														<td><?php echo transform($row['type']); ?></td>
														<td><?php echo $row['amount']; ?></td>
														<td><?php echo $row['date']; ?></td>
														<td><?php echo $row['email']; ?></td>
														<td><?php echo $row['trackerID']; ?></td>
													</tr>
													<?php $count++; }
													} ?>
											  </tbody>
											</table>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
									<div class="card">
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- Deposit</small></p>
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<table class="table">
											  <thead class="thead-dark">
											    <tr>
											      <th scope="col">#</th>
											      <th scope="col">Amount</th>
											      <th scope="col">Date</th>
														<th scope="col">Client</th>
														<th scope="col">State</th>
											      <th scope="col">Tracker ID</th>
											    </tr>
											  </thead>
											  <tbody>
											    <tr>
														<?php
														$count = 1;
														$results->data_seek(0);
														while($row = $results->fetch_assoc()) {
															if($row['type'] == 'cashout') {
														?>
														<tr>
															<th scope="row"><?php echo $count; ?></th>
															<td><?php echo $row['amount']; ?></td>
															<td><?php echo $row['date']; ?></td>
															<td><?php echo $row['email']; ?></td>
															<td><?php echo $row['state']; ?></td>
															<td><?php echo $row['trackerID']; ?></td>
														</tr>
														<?php $count++; }
														} ?>
											    </tr>
											  </tbody>
											</table>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="list-cashin" role="tabpanel" aria-labelledby="list-cashin-list">
									<div class="card">
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- Withdraw</small></p>
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<table class="table">
											  <thead class="thead-dark">
											    <tr>
											      <th scope="col">#</th>
											      <th scope="col">Amount</th>
											      <th scope="col">Date</th>

                            <th scope="col">Client</th>
											      <th scope="col">State</th>
														<th scope="col">Tracker ID</th>
											    </tr>
											  </thead>
											  <tbody>
													<?php
													$count = 1;
													$results->data_seek(0);
													while($row = $results->fetch_assoc()) {
														if($row['type'] == 'cashin') {
													?>
													<tr>
														<th scope="row"><?php echo $count; ?></th>
														<td><?php echo $row['amount']; ?></td>
														<td><?php echo $row['date']; ?></td>
														<td><?php echo $row['email']; ?></td>
														<td><?php echo $row['state']; ?></td>
														<td><?php echo $row['trackerID']; ?></td>
													</tr>
													<?php $count++; }
													} ?>
											  </tbody>
											</table>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="list-transfer" role="tabpanel" aria-labelledby="list-settings-list">
									<div class="card">
										<div class="card-header">
											<p  id="veryThin">Transactions <small id="veryThinSmall">- Transfer</small></p>
										</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<table class="table">
											  <thead class="thead-dark">
											    <tr>
											      <th scope="col">#</th>
											      <th scope="col">Amount</th>
											      <th scope="col">Date</th>
														<th scope="col">Client</th>
														<th scope="col">State</th>
											      <th scope="col">Tracker ID</th>
											    </tr>
											  </thead>
											  <tbody>
											    <tr>
														<?php
														$count = 1;
														$results->data_seek(0);
														while($row = $results->fetch_assoc()) {
															if($row['type'] == 'transfer') {
														?>
														<tr>
															<th scope="row"><?php echo $count; ?></th>
															<td><?php echo $row['amount']; ?></td>
															<td><?php echo $row['date']; ?></td>
															<td><?php echo $row['client']; ?></td>
															<td><?php echo $row['state']; ?></td>
															<td><?php echo $row['trackerID']; ?></td>
														</tr>
														<?php $count++; }
														} ?>
											    </tr>
											  </tbody>
											</table>
												</div>
											</div>
											<!-- <nav aria-label="Page navigation example">
												<ul class="pagination justify-content-center">
													<li class="page-item disabled">
														<a class="page-link" href="#" tabindex="-1">Previous</a>
													</li>
													<li class="page-item"><a class="page-link" href="#">1</a></li>
													<li class="page-item"><a class="page-link" href="#">2</a></li>
													<li class="page-item"><a class="page-link" href="#">3</a></li>
													<li class="page-item">
														<a class="page-link" href="#">Next</a>
													</li>
												</ul>
											</nav> -->
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>


				</div>
				<!-- <div class="col-sm-3">
					<br>
					<div class="jumbotron">
						<p  id="thin">Summary</p><hr>
						<h5 id="label">Total transaction count</h5>
						<p>500</p>
						<h5 id="label">Total cash in</h5>
						<p>500</p>
						<h5 id="label">Total cash out</h5>
						<p>500</p>
						<h5 id="label">Total successful</h5>
						<p>500</p>
						<h5 id="label">Total failed</h5>
						<p>0</p>
					</div>
				</div> -->
			</div>
		</div><br>

		<div class="card footer">
			<div class="card-body">
				Copyright &copy; 2018 Aurbanpay<br>Powered by <a href="https://afkanerd.com">Afkanerd</a>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


		<script>
		function filter() {
			var position = "<?php echo $filterPosition; ?>";
			switch(position) {
				
			}
		}
		</script>
	</body>
</html>
