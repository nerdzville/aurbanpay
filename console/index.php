<?php


require __DIR__ . '/../Conf/Connection.php';

session_start();

if(!isset($_SESSION['user'])) {
	header("Location: ../");
}

if($_GET['logout'] == 'true') {
	unset($_SESSION['user']);
	header("Location: ../");
}

$user = $_SESSION['user'];
$query = "select * from users where SID='$user'";
$results = $conn->query($query) or die("Error executing commands");
$row = $results->fetch_assoc();

$balance = $row['balance'];


?>



<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">

	    <!-- CSS -->
	    <!-- Add Material font (Roboto) and Material icon as needed -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	    <title>Dashboard | Aurbanpay</title>

	    <!-- Add Material CSS, replace Bootstrap CSS -->
	    <link href="../css/main.css" rel="stylesheet">

	</head>

	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="../">AurbanPay</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href=".">Dashboard <span class="sr-only">(current)</span></a>
		      </li>
		      <!-- <li class="nav-item disabled">
		        <a class="nav-link" href="../Docs/index.php">Documentation</a>
		      </li> -->
		      <!-- <li class="nav-item">
		        <a class="nav-link disabled" href="#">Services</a>
		      </li> -->
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Account
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="#">Settings</a>
		          <a class="dropdown-item" href="#">Help</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="./?logout=true">Logout</a>
		        </div>
		      </li>
		    </ul>
		  </div>

		</nav>

		<br>
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-2">
					<br>
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					  <a class="nav-link active" href="index.php" aria-selected="true">Dashboard</a>
					  <a class="nav-link" href="transactions.php" aria-selected="false">Transactions</a>
					  <!--<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
					  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> -->
					</div><hr>
				</div>
				<div class="col-sm-6">
					<br>
					<div class="card">
						<div class="card-header">
							<p  id="veryThin">Dashboard</p>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="card">
										<div class="card-body">
											<div class="container">
												<h5 style="font-size: 16px" class="card-title">Your information</h5>
												<!-- <span style="color: grey">ACCOUNT SID</span><br>
												<span><?php echo $row['SID']; ?></span><br> -->
												<span style="color: grey">PHONE NUMBER</span><br>
												<span><?php echo $row['phonenumber']; ?></span><br><br>
												<span style="color: grey">EMAIL</span><br>
												<span><?php echo $row['email']; ?></span>
												<hr>
												<span><a style="color: grey" href="#">More...</a></span>
											</div>
										</div>
									</div>
									<br>
								</div>
								<!-- <div class="col-sm-6">
									<div class="card">
										<div class="card-body">
											<div class="container">
												<h5 style="font-size: 14px" class="card-title">Your Wallet</h5>
												<span style="color: grey">ACCOUNT BALANCE</span><br>
												<span><?php echo $row['balance']; ?> XAF</span><br>
												<!-- <span style="color: grey">LAST TRANSACTION</span><br>
												<span><?php echo $row['']; ?></span> -->
												<!-- <hr>
												<span><a style="color: grey" href="#">More...</a></span>
											</div>
										</div>
									</div>
									<br>
								</div> -->
							</div>

              <div id="error_div"></div>
							<div class="row">
								<div class="col-sm-6">
									<div class="card">
										<div class="card-header" id="veryThinSmall">Refill your wallet</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<button  data-toggle="modal" data-target="#deposit" class="btn btn-primary">Refill!</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="card">
										<div class="card-header" id="veryThinSmall">Withdraw from your wallet</div>
										<div class="card-body">
											<div class="card">
												<div class="card-body">
													<button  data-toggle="modal" data-target="#withdraw" class="btn btn-primary">Withdraw!</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<br>
					<!-- <p id="veryThinSmall">Control Terminal <small>- see outputs from commands you run</small></p><hr> -->
					<div class="card">
						<div class="card-body">
							<div class="card-title" id="veryThin" style="font-size:20px">Account Balance</div><hr>
							<p id="veryThin"><?php echo $row['balance']; ?> XAF</p>
						</div>
					</div><br>

					<div class="card">
						<div class="card">
							<div class="card-header">Transfer to other client</div>
						</div>
						<div class="card-body">
							<form method="post" action="backend/transaction.php">
								<input type="hidden" name="type" value="transfer">
								<div>
									<label for="amount">Receiver</label>
									<input id="amount" class="form-control" name="receiver" type="text">
								</div><br>
								<div>
									<label for="receiver">Amount</label>
									<div class="row">
									    <div class="col">
									      <input name ="amount" type="text" class="form-control" placeholder="Amount">
									    </div>
									    <div class="col">
									      <input type="submit" onclick="demo('transfer')" class="btn btn-primary" value="Transfer">
									    </div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><br>


		<!-- Modal -->
		<div class="modal fade" id="deposit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="veryThinSmall">Deposit into your wallet</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
				<div>
					<!-- <div>
						<label for="amount">Receiver</label>
						<input id="amount" class="form-control" name="amount" type="text">
					</div><br> -->
					<div>
						<label for="receiver">Amount</label>
						<div class="row">
							<div class="col">
								<input type="text" id="cashout_amount" name="cashout_amount" class="form-control" placeholder="Amount">
							</div>
							<div class="col">
								<select class="form-control" id="exampleFormControlSelect1">
						      <option>MTN Mobile Money</option>
						      <!-- <option>2</option>
						      <option>3</option>
						      <option>4</option>
						      <option>5</option> -->
						    </select>
							</div>
						</div>
					</div>
				</div><hr>
				<div id="modal_progress" class="progress">
				  
				</div>
		      </div>
		      <div class="modal-footer">

		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button onclick="demo('cashout')" type="button" class="btn btn-primary">Deposit</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="withdraw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="veryThinSmall">Withdraw from your wallet</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div>
							<label for="receiver">Amount</label>
							<div class="row">
								<div class="col">
									<input id="cashin_amount" type="text" class="form-control" placeholder="Amount">
								</div>
								<div class="col">
									<select class="form-control" id="exampleFormControlSelect1">
										<option>MTN Mobile Money</option>
										<!-- <option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option> -->
									</select>
								</div>
							</div>
						</div>
					</div>
					<div id="modal_progress2" class="progress">
					</div><hr>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button onclick="demo('cashin')" type="button" class="btn btn-primary">Withdraw</button>
					</div>
				</div>
			</div>
		</div>

		<div class="card footer">
			<div class="card-body">
				Copyright &copy; 2018 Aurbanpay<br>Powered by <a href="https://afkanerd.com">Afkanerd</a>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<!-- <script src="js/functionalities.js"></script> -->

    <script>
      function demo(type) {
        console.log("Beginning a Transaction\nTYPE: " + type);
        var email = "<?php echo $row['email']; ?>";
        var sid = "<?php echo $row['SID']; ?>";

        var modal_success = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Created successfully</strong> Your account has been successfully created! Would redirect you in a bit<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        var modal_danger = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Fill the fields correctly!! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

        var amount, number;
        if(type == "cashout") {
          amount = document.getElementById("cashout_amount").value;
          // number = document.getElementById("cashout_number").value;
          number = "<?php echo $row['phonenumber']; ?>";
        } else if(type == "cashin") {
          amount = document.getElementById("cashin_amount").value;
          number = "<?php echo $row['phonenumber']; ?>";
        } else if(type == "transfer") {
        	amount = document.getElementById("transfer_amount").value;
        	number = document.getElementById("transfer_number").value;
        } else {

        }

        //var reciever = document.getElementById("mobile_reciever").value;
        //var sid = document.getElementById("sid").value;

        console.log("Amount: " + amount + "\nNumber: " + number);

        if(amount.length < 1 || number.length < 9) {
          document.getElementById("error_div").innerHTML = modal_danger;
          return;
        }

        if(type == 'cashout') document.getElementById("modal_progress").innerHTML = '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>';
        else if(type == 'cashin') document.getElementById("modal_progress2").innerHTML = '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>';

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            //document.getElementById("prompt").innerHTML = this.responseText;
            console.log("Transaction made: "+this.responseText);
            if(this.responseText == "Not Enough Balance") {
            	document.getElementById("modal_progress2").innerHTML = "<p style=\"text-align:center; color:red\">Not Enough Balance</p>";
            } else {
            	document.getElementById("modal_progress2").innerHTML = "<p style=\"text-align:center; color:red\">Transaction pending approval, please refresh page after you receive mobile confirmation</p>";
            }
          } else {
           console.log("No response from server");
          }
        };

        xhttp.open("POST", "backend/transaction.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("amount="+amount+"&type="+type);
      }
    </script>
	</body>
</html>
