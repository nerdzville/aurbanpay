<?php

require 'Database.php';
require __DIR__ . '/../configs/index.php';

class Transaction {
  private $database;
  private $set_user, $server_responds, $error_message, $json_return_data, $tracker_id, $balance_update, $call_back_url;
  private $client, $amount, $sid, $email, $type, $status_code;
  public function __construct($client, $amount, $sid, $email, $type, $tracker_id, $call_back_url) {
    $this->database = new Database($GLOBALS['config']->db_state);
    $this->client = $client;
    $this->amount = $amount;
    $this->sid = $sid;
    $this->email = $email;
    $this->type = $type;
    $this->tracker_id = $tracker_id;
    $this->call_back_url = $call_back_url;
  }
  public function execute() {
    if($this->database->run("select * from users where email='$this->email' and sid='$this->sid'")) {
      $this->set_user = "valid";
      switch($this->type) {
        case "cashout":
          if($this->database->run("insert into transactions (client, amount, type, sid, user, tracker_id, call_back_url) values('$this->client', '$this->amount', '$this->type', '$this->sid', '$this->email', '$this->tracker_id', '$this->call_back_url')")) {
            $this->server_responds = "created transaction listing";
             $this->status_code = "102";
          }
          else {
            $this->server_responds = "could not create transaction listing";
            $this->status_code = "503";
          }
        break;

        case "cashin":
        //Calcute against balance!!
          $this->database->run("select balance from users where SID='$this->sid'");
          if($this->database->result->num_rows > 0) {
            $balance = $this->database->result->fetch_assoc()['balance'];
            if($balance - $this->amount < 0) {
              $this->server_responds = "not enough balance";
              $this->status_code = "402";
            } else if($this->database->run("insert into transactions (client, amount, type, sid, user, tracker_id, call_back_url) values('$this->client', '$this->amount', '$this->type', '$this->sid', '$this->email', '$this->tracker_id', '$this->call_back_url')")) {
              $this->server_responds = "created transaction listing";
                /*if($this->database->run("update users set balance=balance - $this->amount where SID='$this->sid'")) {
                  $this->balance_update = "successful";
                  $this->status_code = "102";
                }
                else {
                  $this->server_responds = "could not create transaction listing";
                  $this->status_code = "500";
                }*/
              }
              else {
                $this->server_responds = "failed";
                $this->status_code = "500";
              }
          }

        break;

        default:
        $this->error_message = "invalid type '$this->type'";
        $this->status_code = "405";
        break;
      }
    }
    else {
      $this->set_user = "not valid";
      $this->status_code = "403";
    }
  }

  public function results() {
    $json_php_array = array("statusCode"=>$this->status_code, "serverResponse"=>$this->server_responds, "uniqueId"=>$this->tracker_id);
    if(!empty($this->error_message)) $json_php_array['error_message'] = $this->error_message;
    if($this->server_responds == "successful") {
      $json_php_array["transaction_state"] = "pending";
      //$json_php_array["balance_update"] = $this->balance_update;
    }
    $this->json_return_data = json_encode($json_php_array);
    return $this->json_return_data;
  }
}


?>
