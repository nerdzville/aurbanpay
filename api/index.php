<?php

require 'classes/Transaction.php';
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $client = $_POST['client'];
  $amount = $_POST['amount'];
  $sid = $_POST['sid'];
  $email = $_POST['email'];
  $type = $_POST['type'];
  $tracker_id = $_POST['trackerId'];
  $call_back_url = $_POST['callbackUrl'];

  $transaction = new Transaction($client, $amount, $sid, $email, $type, $tracker_id, $call_back_url);
  $transaction->execute();
  $read_data = $transaction->results();
  echo $read_data;
  return $read_data;
}

else if(defined('STDIN')) {
  if($argv[1] == "test") {
    $client = 652156811;
    $amount = 500;
    //$sid = "$2y$10$9FgvZk8jkSlb5nY7hk3av.68jusoX8Ocr7Awk9fayC9PjYbH2Hj9m"; //online
    $sid = "2qMD3wabs4BbGqR0I0mM82rVI0pFwdQh";
    $email = "dev@afkanerd.com";
    $type = $argv[2];
    $tracker_id = "123456";
    $call_back_url = "https://afkanerd.com";
  } else {
    $client = $argv[1];
    $amount = $argv[2];
    $sid = $argv[3];
    $sid = "7SzH7vHjL2qZqJCDr67ZJCEDGKqnfhbm";
    $email = $argv[4];
    $type = $argv[5];
    $tracker_id = $argv[6];
    $call_back_url = $argv[7];
  }
  $transaction = new Transaction($client, $amount, $sid, $email, $type, $tracker_id, $call_back_url);
  $transaction->execute();
  $read_data = $transaction->results();
  print("Data from Transactions: ".$read_data."\n");

  switch($transaction->status_code) {
    case "402":
    postCallBack($call_back_url, $transaction->status_code, $tracker_id);
    break;
  }


}

function postCallBack($callbackUrl, $statusCode, $trackerID) {
  print("Calling back: $callbackUrl\n");
  $serverResponse;
  if($statusCode == "200") $serverResponse = "successful";
  else $serverResponse = "failed";
  //if(empty($) || !isset($_url)) return;
  $return = array('statusCode'=>$statusCode, 'serverResponse'=>$serverResponse, 'trackerId'=>$trackerID);
  $url = $callbackUrl;
  $api_responds = json_encode($return);

  $ch = curl_init($url);
  curl_setopt( $ch, CURLOPT_POST, 1);
  curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_responds);
  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt( $ch, CURLOPT_HEADER, array("Content-type: application/json"));
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

  $response = curl_exec( $ch );
  print("Server returned: $response\n");
}


?>
