<?php

session_start();

if(isset($_SESSION['user'])) {
	header("Location: ../console/");
}

?>


<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">
    <!-- Add Material font (Roboto) and Material icon as needed -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/sidebar.css">
    <!-- <script src="../material/js/material.js"></script> -->
		<script src="js/functionalities.js" type="text/javascript">

		</script>
    <title>Signup | Aurbanpay</title>
  </head>
  <style>
  @import url('https://fonts.googleapis.com/css?family=Julius+Sans+One');
  </style>
<body style="height: 100%; background: url('../images/background.jpg') center / cover">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href=".">AurbanPay</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item active">
          <a class="nav-link" href="Doctor">I am a Doctor <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="EHR/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            EHR
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li> -->
      </ul>
      <div class="form-inline my-2 my-lg-0">
        <div>
          <div id="signin_error"></div>
          <div class="form-row">
            <div class="col">
              <label style="color: white" for="signin_email">Phone Number</label>
              <input required id="signin_phonenumber" type="text" class="form-control" placeholder="Phone Number">
            </div>
            <div class="col">
              <label style="color: white" for="signin_password">Password</label>
              <input required id="signin_password" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="col">
              <label style="color: white" for="signin_btn"><a href="#">Forgot Password</a></label><br>
              <button id="signin_btn" onclick="signin()" class="btn btn-info form-control">Signin</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-8">

      </div>
      <div class="col-sm-4" style="height: 600px; background-color: white;">
        <br>
        <div class="container-fluid">
					<div id="signup_error"></div>
          <div class="card" style="background-color: #ededed;">
            <div class="card-body">
              <h4 class="card-title">Signup is Free!</h4><hr>
              <div>
								
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" required class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                  <small id="email" name="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="text" required class="form-control" id="phonenumber" aria-describedby="emailHelp" placeholder="Enter phone number">
                  <small id="phonenumber" name="phonenumber" class="form-text text-muted">We'll never share your number with anyone else.</small>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" required class="form-control" id="password" placeholder="Password">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Re-Password</label>
                  <input type="password" name="re-password" required class="form-control" id="re-password" placeholder="Re-Password">
                </div>

                <button onclick="signup()" class="btn btn-primary">Join</button>
              </div>
            </div>
          </div>
          <br><br>
        </div>
      </div>
    </div>
  </div>

  <div class="card footer">
    <div class="card-body">
      <p style="text-align: center"> All rights reserved &copy; 2018 Aurbanpay<br>Powered by <a href="https://afkanerd.com">Afkanerd</a></p>
    </div>
  </div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</body>

</html>
