<?php

require __DIR__ . '/../../api/classes/Transaction.php';

session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $email = $_POST['email'];
  $phonenumber = $_POST['phonenumber'];
  $password = $_POST['password'];
  $sid = generateRandomString();
  $password_hash = password_hash($password, PASSWORD_DEFAULT);

  $database = new Database($config->db_state);
  $database->run("insert into users (email, phonenumber, password_hash, SID) values ('$email', '$phonenumber', '$password_hash', '$sid')");
  if($database->result === TRUE) {
    $_SESSION['user'] = $sid;
    echo 'true';
  }
  else echo 'Email already exist, please try another, if it persist please contact admin';
}

function generateRandomString($length = 32) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}


?>
