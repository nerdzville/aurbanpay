function signin() {
  console.log("in signin js");
  var modal_success = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Welcome</strong> You have successfully logged in! Would redirect you in a bit<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  var modal_danger = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Your password do not match! Try again.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  var phonenumber = document.getElementById("signin_phonenumber").value;
  var password = document.getElementById("signin_password").value;
  // var re_password = document.getElementById("re-password").value;

  //email = "dev1@test.com";
  //password = "asshole";
  if(!validatePhonenumber(phonenumber)) {
    document.getElementById("signin_error").innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Not a valid email<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    return;
  }
  if(password.length < 1) {
    document.getElementById("signin_error").innerHTML = modal_danger;
    return;
  }
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText.replace(/^\s+|\s+$/g, "") == 'true') {
            console.log("Should redirect now");
            document.getElementById("signin_error").innerHTML = modal_success;
            window.setTimeout(function(){ window.location.replace("../console/"); },3000);
          } else {
            document.getElementById("signin_error").innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> '+this.responseText+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
          }
     } else {
       console.log("No response from server");
     }
  };
  xhttp.open("POST", "backend/login.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("phonenumber="+phonenumber+"&password="+password);
}

function signup() {
  console.log("in signup js");
  var modal_success = '<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Created successfully</strong> Your account has been successfully created! Would redirect you in a bit<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  var modal_danger = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Your password do not match! Try again.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  var email = document.getElementById("email").value;
  var phonenumber = document.getElementById("phonenumber").value;
  var password = document.getElementById("password").value;
  var re_password = document.getElementById("re-password").value;

  console.log("Email: "+email+"\nPhone Number: " + phonenumber + "\nPassword: "+password+"\nRe-password: "+re_password+"\n");

  //email = "dev13@test.com";
  //password = re_password = "asshole";
  if(!validateEmail(email)) {
    document.getElementById("signup_error").innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Not a valid email<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    return;
  }
  if(!validatePhonenumber(phonenumber)) {
    document.getElementById("signup_error").innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> Not a valid email<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    return;    
  }
  if(password != re_password || password.length < 1) {
    document.getElementById("signup_error").innerHTML = modal_danger;
    return;
  }
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if(this.responseText.replace(/^\s+|\s+$/g, "") == 'true') {
            console.log("Should redirect now");
            document.getElementById("signup_error").innerHTML = modal_success;
            window.setTimeout(function(){ window.location.replace("../console/"); },3000);
          } else {
            document.getElementById("signup_error").innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> '+this.responseText+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
          }
     } else {
       console.log("No response from server");
     }
  };
  xhttp.open("POST", "backend/signup.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("email="+email+"&phonenumber="+phonenumber+"&password="+password);
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function validatePhonenumber(phonenumber) {
  return true;
}
