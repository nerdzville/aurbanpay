<?php

require 'classes/Transaction.php';
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $client = $_POST['client'];
  $amount = $_POST['amount'];
  $sid = $_POST['sid'];
  $email = $_POST['email'];
  $type = $_POST['type'];
  $tracker_id = $_POST['trackerId'];
  $call_back_url = $_POST['callbackUrl'];

  $transaction = new Transaction($client, $amount, $sid, $email, $type, $tracker_id, $call_back_url);
  $transaction->execute();
  $read_data = $transaction->results();
  echo $read_data;
  return $read_data;
}

else if(defined('STDIN')) {
  if($argv[1] == "test") {
    $client = 652156811;
    $amount = 500;
    //$sid = "$2y$10$9FgvZk8jkSlb5nY7hk3av.68jusoX8Ocr7Awk9fayC9PjYbH2Hj9m"; //online
    $sid = "2qMD3wabs4BbGqR0I0mM82rVI0pFwdQh";
    $email = "dev@afkanerd.com";
    $type = $argv[2];
    $tracker_id = "123456";
    $call_back_url = "https://afkanerd.com";
  } else {
    $client = $argv[1];
    $amount = $argv[2];
    $sid = $argv[3];
    $sid = "7SzH7vHjL2qZqJCDr67ZJCEDGKqnfhbm";
    $email = $argv[4];
    $type = $argv[5];
    $tracker_id = $argv[6];
    $call_back_url = $argv[7];
  }
  $transaction = new Transaction($client, $amount, $sid, $email, $type, $tracker_id, $call_back_url);
  $transaction->execute();
  $read_data = $transaction->results();
  print($read_data."\n");

  return $read_data;
  //postCallBack($call_back_url, $transaction->status_code, $tracker_id);


}

function postCallBack($callbackUrl, $statusCode, $trackerID) {
    $url = $callbackUrl;
    $data = json_encode(array('statusCode' => $statusCode, 'trackerID' => $trackerID));

    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/json\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }

    var_dump($result);
}


?>
