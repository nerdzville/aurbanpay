<?php

class Config {
  public $db_state = "online";
}
$config = new Config;

if(isset($_GET['--database_state'])) {
  $db_state = $_GET['--database_state'];
  switch($db_state) {
    case "online":
    case "offline":
    $config->db_state = $db_state;
    echo("Database state set to: $db_state<br>");
    break;

    default:
    echo "Unknown Database State '$db_state'<br>";
    break;
  }
}

else if(defined('STDIN')) {
  $db_state = $argv[2];
  switch($db_state) {
    case "online":
    case "offline":
    $config->db_state = $db_state;
    print("Database state set to: $db_state\n");
    break;

    default:
    print("Unknown Database State '$db_state'\n");
    break;
  }
}

?>
