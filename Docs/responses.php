<?php

require __DIR__ . '/../Conf/Connection.php';

$query1 = "select * from response_codes";
$results1 = $conn->query($query1) or die("Error executing commands");


?>



<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">

	    <!-- CSS -->
	    <!-- Add Material font (Roboto) and Material icon as needed -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	    <title>Responses - Afkanerd</title>

	    <!-- Add Material CSS, replace Bootstrap CSS -->
	    <link href="css/main.css" rel="stylesheet">

	</head>

	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">GINA</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="../">Dashboard <span class="sr-only">(current)</span></a>
		      </li>
		      <!-- <li class="nav-item active">
		        <a class="nav-link" href=".">Documentation</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link disabled" href="#">Services</a>
		      </li>
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Account
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="#">Settings</a>
		          <a class="dropdown-item" href="#">Help</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="#">Logout</a>
		        </div>
		      </li> -->
		    </ul>
		  </div>

		</nav>

		<br>
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-2">
					<br>
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					  <a class="nav-link" href="." aria-selected="true">API</a>
					  <a class="nav-link active" href="responses.php" aria-selected="false">Responses</a>
					  <!--<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
					  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> -->
					</div>
				</div>
				<div class="col-sm-10">
					<br>
					<div class="card">
						<div class="card-header">
							<p id="veryThin">Responsis - Documentation</p>
						</div>
						<div class="card-body">
              <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Code</th>
                    <th scope="col">Message</th>
                    <th scope="col">Cashout</th>
                    <th scope="col">Cashin</th>
                  </tr>
                </thead>
                <tbody>
									<?php
									$count = 1;
									while($row1 = $results1->fetch_assoc()) {
									?>
                  <tr>
                    <th scope="row"><?php echo $row1['code']; ?></th>
                    <td><?php echo $row1['message']; ?></td>
                    <td><?php echo $row1['cashout']; ?></td>
										<td><?php echo $row1['cashin']; ?></td>
                  </tr>
									<?php
									$count++;
									} ?>
                </tbody>
              </table>
            </div>
					</div>
				</div>
					<br>
				</div>
			</div>
		</div><br>

		<div class="card footer">
			<div class="card-body">
				Copyright &copy; 2018 <a href="https://afkanerd.com">Afkanerd Inc</a>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>
