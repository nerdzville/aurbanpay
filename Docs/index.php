<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta content="initial-scale=1, shrink-to-fit=no, width=device-width" name="viewport">

	    <!-- CSS -->
	    <!-- Add Material font (Roboto) and Material icon as needed -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	    <title>API - Afkanerd</title>

	    <!-- Add Material CSS, replace Bootstrap CSS -->
	    <link href="css/main.css" rel="stylesheet">

	</head>

	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">GINA</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="../">Dashboard <span class="sr-only">(current)</span></a>
		      </li>
		      <!-- <li class="nav-item active">
		        <a class="nav-link" href="#">Documentation</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link disabled" href="#">Services</a>
		      </li>
					<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Account
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="#">Settings</a>
		          <a class="dropdown-item" href="#">Help</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="#">Logout</a>
		        </div>
		      </li> -->
		    </ul>
		  </div>

		</nav>

		<br>
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-2">
					<br>
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					  <a class="nav-link active" href="index.html" aria-selected="true">API</a>
					  <a class="nav-link" href="responses.php" aria-selected="false">Responses</a>
					  <!--<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
					  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> -->
					</div>
				</div>
				<div class="col-sm-10">
					<br>
					<div class="card">
						<div class="card-header">
							<p  id="veryThin">API - Documentation</p>
						</div>
						<div class="card-body">
              <div class="row">
                <div class="col-sm-2">
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Cashout</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Cashin</a>
                  </div>

                </div>
                <div class="col-sm-10">
                  <div class="card">
                    <div class="card-body">
                      <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                          <h4 id="sub">Cashout</h4>
                          <p>To carry out a transaction for cashout from a clien'ts number, use a POST method to handle the transaction with the method.
														<h5>API call URL</h5>
														<p>https://gsmtools.afkanerd.com/api/
														<h5>List of Parameters</h5>
														<ul>
															<li>client - takes the receiving client's number</li>
															<li>amount - takes the amount to be withdrawn by the client</li>
															<li>
																sid - your sid <small>can be found on your dashboard</small>
															</li>
															<li>email - your email for this account</li>
															<li>type - in this case it is "cashout"</li>
															<li>tracker_id - your very own generated unique code for this transaction</li>
															<li>
																callbackurl - after the transaction is done, the results would be sent to this url<small>All return data is JSON</small>
															</li>
														</ul>
													</p>
                          <div class="jumbotron terminal">
                            https://gmstools.afkanerd.com/api/?client=number&amount=amount&sid=sid&email=email&type=cashout&tracker_id=11235&callbackurl=https://example.com
                          </div><br><br>
                          <p>The API should send an immediate response using JSON format, it contains information related to the current state of the transaction.</p>
                          <div class="jumbotron terminal">
                            {<br>
                              &nbsp; &nbsp; &nbsp;"statusCode" : "102",<br>
                              &nbsp; &nbsp; &nbsp;"serverResponse" : "created transaction listing",<br>
                              &nbsp; &nbsp; &nbsp;"uniqueId" : null<br>
                            }<br>
                          </div><br><br>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                          <h4 id="sub">Cashin</h4>
                          <p>To do a cashin to a clien'ts number, use a POST method to handle the transaction with the method.</p>
													<h5>API call URL</h5>
													<p>https://gsmtools.afkanerd.com/api/
													<h5>List of Parameters</h5>
													<ul>
														<li>client - takes the receiving client's number</li>
														<li>amount - takes the amount to be received by the client</li>
														<li>
															sid - your sid <small>can be found on your dashboard</small>
														</li>
														<li>email - your email for this account</li>
														<li>type - in this case it is "cashin"</li>
														<li>tracker_id - your very own generated unique code for this transaction</li>
														<li>
															callbackurl - after the transaction is done, the results would be sent to this url<small>All return data is JSON</small>
														</li>
													</ul>
                          <div class="jumbotron terminal">
                            https://gmstools.afkanerd.com/api/?client=number&amount=amount&sid=sid&email=email&type=cashin&tracker_id=11235&callbackurl=https://example.com/
                          </div><br><br>
                          <p>The API should send an immediate response using JSON format, it contains information related to the current state of the transaction.</p>
                          <div class="jumbotron terminal">
                            {<br>
                              &nbsp; &nbsp; &nbsp;"statusCode" : "102",<br>
                              &nbsp; &nbsp; &nbsp;"serverResponse" : "created transaction listing",<br>
                              &nbsp; &nbsp; &nbsp;"uniqueId" : null<br>
                            }<br>
                          </div><br><br>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
					</div>
				</div>
					<br>
				</div>
			</div>
		</div><br>

		<div class="card footer">
			<div class="card-body">
				Copyright &copy; 2018 <a href="https://afkanerd.com">Afkanerd Inc</a>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>
